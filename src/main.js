import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import './i18n';

// Import asset
import './main.css';

const addModuleHTML = () => {
  const container = document.createElement('div');
  container.id = 'abtasty-module';
  document.body.appendChild(container);
  ReactDOM.render(<App/>, container);
};

const start = () => {
  addModuleHTML();
  window._ab.ready();
};

start();
