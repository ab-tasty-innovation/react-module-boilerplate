import React from 'react';
import { withTranslation } from 'react-i18next';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: 'Hello World!',
    };
  }

  render() {
    const { t } = this.props
    return (
      <React.Fragment>
        <h1>{t(this.state.message)}</h1>
      </React.Fragment>
    );
  }
}

export default withTranslation()(App);
