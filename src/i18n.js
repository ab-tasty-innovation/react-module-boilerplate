import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import translationsEn from '../translations/translations_en_US.json';
import translationsFr from '../translations/translations_fr.json';

const getLanguage = () => {
  let language = 'en';
  if (window._ab && window._ab.profile) {
    language = window._ab.profile.lang || language;
  }
  return language;
};

const language = getLanguage();

const resources = {
  en: { translation: translationsEn },
  fr: { translation: translationsFr },
};

i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: language,
    keySeparator: false,
    interpolation: {
      escapeValue: false,
    },
    fallbackLng: {
      'default': ['en']
    }
  });

export default i18n;
