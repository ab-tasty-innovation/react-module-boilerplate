#!/bin/bash
if [ $? -eq 0 ]; then
  while [[ "$#" -gt 0 ]]; do
    case $1 in -p | --package)
      case "$2" in bootstrap)
        npm install react-bootstrap bootstrap -s
        vi -c "%s/Import asset/Import asset\rimport\ 'bootstrap\/dist\/css\/bootstrap.min.css';/g | write | quit" src/main.js
        ;;
      commonui)
        npm i @abtastytech/pulsar-common-ui -s
        vi -c "%s/Import asset/Import asset\rimport '@abtastytech\/pulsar-common-ui\/static\/fonts.css';\rimport '@abtastytech\/pulsar-common-ui\/static\/reset.css';/g | write | quit" src/main.js
        ;;
      *)
        echo "Packages should be (bootstrap|commonui)"
        ;;
      esac
      ;;
    --projectName)
      sed -i '' "s/<%= PROJECT_NAME %>/$2/g" package.json
      sed -i '' "s/<%= PROJECT_NAME %>/$2/g" README.md
      ;;
    --projectDescription)
      sed -i '' "s/<%= PROJECT_DESCRIPTION %>/$2/g" package.json
      ;;
    --projectAuthor)
      sed -i '' "s/<%= PROJECT_AUTHOR %>/$2/g" package.json
      ;;
    esac
    shift
  done
  echo "Install dependancies"
  npm install
  rm -rf .git/
  git init
  echo "Ready to start"
  exit 0
else
  echo "NPM is required"
  exit 1
fi
