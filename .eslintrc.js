module.exports = {
  extends: ["airbnb-base", "plugin:react/recommended"],
  env: {
    es6: true,
    browser: true
  },
  parserOptions: {
    sourceType: "module"
  },
  rules: {
    "no-underscore-dangle": "off",
    "no-new": "off"
  }
};
