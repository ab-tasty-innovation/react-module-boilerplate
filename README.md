# <%= PROJECT_NAME %>


## Preview your module
Launch your module in a new window and watch for changes.
```bash
npm start
```

## Get / Update the translations
To get or update your project translations run
```bash
npm run update:translations
```
