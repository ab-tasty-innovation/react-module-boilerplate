/**
 * Based on the work of the widget team
 */
'use strict';

const { google } = require('googleapis');
const fs = require('fs');
const path = require('path');

// TODO: replace with your actual API key
const sheetsAPI = google.sheets({
  version: 'v4',
  auth: 'API_KEY'
});

/**
 * Get translations from google spreadsheet:
 */
function getTranslations() {
  // TODO: replace with the correct spreadsheet id and range (https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/get)
  sheetsAPI.spreadsheets.values.get({
    spreadsheetId: 'spreadsheetId',
    range: 'A1:D',
  }).then(res => {
    if (!res.data && !res.data.values) return;

    const translations = {
      en: {},
      fr: {},
    };

    const fileMap = {
      en_US: 'en',
      fr: 'fr'
    }

    res.data.values.forEach(([key, en, fr]) => {
      translations.en[key] = en;
      translations.fr[key] = fr;
    });

    return Promise.all(Object.entries(fileMap).map(([langName, langKey]) => {
      return writeTranslation(translations[langKey], langName)
    }))
  }).catch(err => {
    console.error('The API returned an error.', err);
  })
}

function writeTranslation(translation, lang) {
  const content = JSON.stringify(translation, undefined, 2)
  const filename = `translations_${lang}.json`
  return fs.promises.writeFile(path.join(__dirname + "/..", filename), content)
    .then(() => console.log(`wrote translation ${filename}`))
}

getTranslations();
