const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './src/main.js',
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|png|jp(e*)g|svg)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 100000,
                    }
                }]
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([{
            from: 'conf.json',
            to: './'
        }, {
            from: 'README.md',
            to: './readme.md'
        }])
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'script.js'
    }
};
